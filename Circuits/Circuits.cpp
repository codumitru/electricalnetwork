// Circuits.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>// Circuits.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <string>
#include <algorithm>
#include <ctime>

using namespace std;

struct Circuit
{
	uint32_t value;
	uint32_t nominalValue;
	uint8_t phase;
	uint8_t isOn;
	uint32_t totalTimeOn;
};


const signed char CircuitsNumber = 16;
Circuit circuits[CircuitsNumber];

const signed char PhasesNumber = 3;
uint32_t phasesTotals[PhasesNumber];

const uint8_t MAX_PHASE_AMPERAGE = 20;
const uint32_t MAX_TIME_ON = 15000;


int main()
{
	for (int i = 0; i < 16; i++)
	{
		circuits[i].isOn = 0;
		circuits[i].totalTimeOn = 0;
	}
	for (int i = 0; i < 5; i++)
	{
		circuits[i].phase = 1;
	}
	for (int i = 5; i < 10; i++)
	{
		circuits[i].phase = 2;
	}
	for (int i = 10; i < 16; i++)
	{
		circuits[i].phase = 3;
	}

	for (int j = 1; j < 100; j++)
	{
		clock_t initialTime = clock();

		float diff = 0.02;

		do
		{
			/*	cout << diff << endl;
				cout << clock() << endl;*/
			diff = (float)(clock() - initialTime) / CLOCKS_PER_SEC;

		} while (diff < 0.02);

		cout << "-----------" << endl;
		for (int i = 0; i < CircuitsNumber; i++)
		{
			circuits[i].value = rand() % 10;

			cout << circuits[i].value << circuits[i].phase << endl;
		}
	}
}

void switchCircuitOff(uint8_t phaseIndex, uint32_t phaseTotalValue)
{
	bool phaseOk = false;
	uint32_t totalCircuitsValue = 0;

	for (uint8_t i = 0; i < CircuitsNumber; i++)
	{
		if (circuits[i].phase == phaseIndex)
		{
			totalCircuitsValue += circuits[i].value;

			if (phaseTotalValue - totalCircuitsValue > MAX_PHASE_AMPERAGE)
			{
				circuits[i].isOn = 0;
			}
			else if (phaseTotalValue - totalCircuitsValue + circuits[i].value > MAX_PHASE_AMPERAGE)
			{
				circuits[i].isOn = 0;
			}
			else
			{
				circuits[i].totalTimeOn += 1;
			}
		}
		else
		{
			totalCircuitsValue = 0;
		}
	}
}

void switchCircuitOn(int phaseIndex, int phaseTotalValue)
{
	bool phaseOk = false;

	while (!phaseOk)
	{
		for (int i = 0; i < CircuitsNumber; i++)
		{
			circuits[i].isOn = 0;
			if (circuits[i].phase == phaseIndex && phaseTotalValue - circuits[i].phase <= MAX_PHASE_AMPERAGE)
			{
				circuits[i].isOn = 0;
				phaseOk = true;
				break;
			}
		}
	}
}

void analyzeElectricalNetwork(bool valueProcessed)
{
	if (!valueProcessed)
		return;

	for (int i = 0; i < PhasesNumber; i++)
	{
		if (phasesTotals[i] > MAX_PHASE_AMPERAGE)
		{
			switchCircuitOff(i, phasesTotals[i]);
		}
		else if (phasesTotals[i] == MAX_PHASE_AMPERAGE)
		{
			for (uint8_t j = 0; j < CircuitsNumber; j++)
			{
				if (circuits[j].phase == i)
				{
					circuits[j].totalTimeOn += 1;
				}
			}
		}
		else
		{
			switchCircuitOn(i, phasesTotals[i]);
		}
	}
}

#include <ctime>

using namespace std;

const signed char CircuitsNumber = 16;
int circuits[CircuitsNumber][3];

const int VALUE = 0;
const int LAST_UPDATED = 1;
const int PHASE = 2;

const signed char PhasesNumber = 3;
int phasesTotals[PhasesNumber];

const int MaxPhaseAmperage = 20;


int main()
{
	for (int i = 0; i < 5; i++) 
	{
		circuits[i][LAST_UPDATED] = 0;
		circuits[i][PHASE] = 1;
	}
	for (int i = 5; i < 10; i++)
	{
		circuits[i][LAST_UPDATED] = 0;
		circuits[i][PHASE] = 2;
	}
	for (int i = 10; i < 16; i++)
	{
		circuits[i][LAST_UPDATED] = 0;
		circuits[i][PHASE] = 3;
	}
	
	for ( int j = 1; j < 1000; j++ ) 
	{
		clock_t initialTime = clock();

		float diff = 0.02;

		do
		{
			cout << diff << endl;
			cout << clock() << endl;
			diff = (float)(clock() - initialTime) / CLOCKS_PER_SEC;

		} while (diff < 0.02);

		cout << "-----------" << endl;
		for (int i = 0; i < CircuitsNumber; i++)
		{
			circuits[i][VALUE] = rand() % 10;

			cout << circuits[i][VALUE] << circuits[i][LAST_UPDATED] << circuits[i][PHASE] << endl;
		}
	}
}

void switchCircuitOff(int phaseIndex, int phaseTotalValue)
{
	bool phaseOk = false;

	while (!phaseOk)
	{
		for (int i = 0; i < CircuitsNumber; i++)
		{
			if (circuits[i][PHASE] == phaseIndex && phaseTotalValue - circuits[i][VALUE] <= MaxPhaseAmperage && circuits[i][LAST_UPDATED] == 0)
			{
				circuits[i][VALUE] = 0;
				circuits[i][LAST_UPDATED] = 1;
				phaseOk = true;
				break;
			}
		}
	}
}

void analyzeElectricalNetwork(bool valueProcessed)
{
	if (!valueProcessed)
		return;

	for (int i = 0; i < PhasesNumber; i++)
	{
		if(phasesTotals[i] >= MaxPhaseAmperage)
		{
			switchCircuitOff(i, phasesTotals[i]);
		}
	}
}
